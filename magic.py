import board
import zipfile
from itertools import product

def extract_zip(filename, password):

    zipFile = zipfile.ZipFile(filename)

    try:
        zipFile.extractall(pwd=password.encode())

        return True

    except:

        return False


def get_alphabet_combinations(length):

    return [''.join(pw) for pw in product('qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890', repeat = length)]    


def dictionarySpell(zipFile = "", dictionaryFile = ""):

    result = board.labelCollection()["password-not-found"]

    dictionary_file = open(dictionaryFile, 'r') 
    dictionary_file_line = dictionary_file.readlines() 

    if (extract_zip(zipFile, "")):
        result= board.labelCollection()["password-not-required"]

    else:

        for line in dictionary_file_line: 

            if (extract_zip(zipFile, line.strip())):
                result = line.strip()
                break

    return result


def bruteForceSpell(zipFile):

    result = board.labelCollection()["password-not-found"]
    word_length = 0

    while (result == board.labelCollection()["password-not-found"]):

        print(board.labelCollection()["password-length"] + str(word_length))

        word_combinations = get_alphabet_combinations(word_length)

        for word in word_combinations:
            
            if (extract_zip(zipFile, word)):
                result = word

                if (word_length == 0):
                    result = board.labelCollection()["password-not-required"]

                break

        word_length += 1

    return result