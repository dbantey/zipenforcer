def labelCollection():

    textfile = {

        "main-programTitle": "ZIP Enforcer", 
        "main-programVersion": "v0.62",
        "main-menuTitle": "Menu",
        "main-menuSelectionMessage": "What would you like to do? ",
        "main-menuSelectionInvalidMessage": "Invalid input. Please try again.",
        "main-menuItems": [
            "Dictionary",
            "Brute force",
            "Help",
            "Exit"
            ],

        "dictionary-message": "A form of brute force attack technique for defeating a cipher or \nauthentication mechanism by trying to determine its decryption key or \npassphrase by trying thousands or millions of likely possibilities, \nsuch as words in a dictionary or previously used passwords, often from\nlists obtained from past security breaches.",
        "dictionary-menuSelectionMessage": "What would you like to do? ",
        "dictionary-menuSelectionInvalidMessage": "Invalid input. Please try again.",
        "dictionary-begin": "Matching passwords from dictionary file...",
        "dictionary-begin-invalid": "Please verify both ZIP and dictionary files are selected.",
        "dictionary-menuItems": [
            "ZIP File: <Select to enter>",
            "Dictionary File: <Select to enter>",
            "Begin Process",
            "Main Menu",
            "Exit"
            ],

        "brute-force-message": "An attacker submitting many passwords or passphrases with the hope \nof eventually guessing correctly. The attacker systematically checks \nall possible passwords and passphrases until the correct one is found. \nAlternatively, the attacker can attempt to guess the key which is \ntypically created from the password using a key derivation function. \nThis is known as an exhaustive key search.",
        "brute-force-menuSelectionMessage": "What would you like to do? ",
        "brute-force-menuSelectionInvalidMessage": "Invalid input. Please try again.",
        "brute-force-begin": "Matching passwords using brute force...",
        "brute-force-begin-invalid": "Please verify if ZIP file is selected.",
        "brute-force-menuItems": [
            "ZIP File: <Select to enter>",
            "Begin Process",
            "Main Menu",
            "Exit"
            ],

        "help-message": "Everyone is guilty of an indictable offence and liable to imprisonment for \na term of not more than 10 years, or is guilty of an offence punishable on\nsummary conviction who, fraudulently and without colour of right, (a) \nobtains, directly or indirectly, any computer service; (b) by means of an\nelectro-magnetic, acoustic, mechanical or other device, intercepts or causes \nto be intercepted, directly or indirectly, any function of a computer system; \n(c) uses or causes to be used, directly or indirectly, a computer system with \nintent to commit an offence under paragraph (a) or (b) or under section 430 in \nrelation to computer data or a computer system; or uses, possesses, traffics \nin or permits another person to have access to a computer password that would \nenable a person to commit an offence under paragraph (a), (b) or (c). For more \ninformation visit: https://laws-lois.justice.gc.ca/eng/acts/C-46/section-342.1.html\n",
        "help-menuSelectionMessage": "Where to go from here? ",
        "help-menuSelectionInvalidMessage": "Invalid input. Please try again.",
        "help-menuItems": [
            "Main Menu",
            "Exit"
            ],

        "error-label": "Error: ",
        "error-type-1": "Something is wrong",
        "exit-message": "Goodbye!",
        "invalid-file": "Invalid file name. Please try again.",
        "file-not-found": " not found. Please try again.",
        "file-selected": " successfully selected.",
        "input-file-name": "Please input a file name: ",
        "password-label": "Password: ",
        "password-length": "Password length: ",
        "password-not-found": "<Not Found>",
        "password-not-required": "<Not Required>",
        "stop-progress": "Press <CTRL + C> to stop process",
        "select-file": "<Select to enter>",
        "separator": "-------------------------------",

        }

    return textfile