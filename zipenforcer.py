
import board
import brushe
import paintz

def menu(hideTitle = False):

    if (not hideTitle):
        paintz.titlePress(board.labelCollection()["main-programTitle"], board.labelCollection()["main-programVersion"])
    
    paintz.itemPress(board.labelCollection()["main-menuTitle"], board.labelCollection()["main-menuItems"])

    menuSelection(paintz.menuInput(board.labelCollection()["main-menuSelectionMessage"]), board.labelCollection()["main-menuItems"], 
        board.labelCollection()["main-menuSelectionMessage"], board.labelCollection()["main-menuSelectionInvalidMessage"])

def menuSelection(input = "", menuItems = [], menuSelectionMessage = "", menuSelectionInvalidMessage = ""):
    if (paintz.itemPressValidate(input, menuItems)):
            paintz.separator()
            paintz.titlePress(menuItems[int(input) - 1])
            brushe.menuSwitcher(menuItems[int(input) - 1])
    else:
        print(menuSelectionInvalidMessage)
        menuSelection(paintz.menuInput(menuSelectionMessage), menuItems, menuSelectionMessage, menuSelectionInvalidMessage)


if __name__ == '__main__':
    menu()