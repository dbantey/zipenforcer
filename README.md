# README #

git clone https://dbantey@bitbucket.org/dbantey/zip-forcer.git

A concept application designed to break password protected zip files for educational purposes only.

### What is this repository for? ###

* 0.62

### How do I get set up? ###

* Download and install the latest [Python](https://www.python.org/downloads/)
* Run "python zipenforcer.py"

### Note ###

* Unauthorized access to private information is illegal. Please use code responsibily.