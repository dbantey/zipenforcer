import board
import os.path

def requiredFileValidator(file = []):

    result = all(file)

    return result

def itemPress(itemTitle = "", itemList = []):

    if (itemTitle):
        print(itemTitle)
        separator()
    
    if (len(itemList)):
        for index in range(len(itemList)):
            print(str(index + 1) + ". " + str(itemList[index]))
        separator()


def itemPressValidate(input, itemList):

    result = input and input.isdigit() and int(input) > 0 and int(input) <= len(itemList)

    return result


def menuInput(message = ""):

    inputMessage = ""

    if (message):
        inputMessage = input(message)
    
    return inputMessage


def pathValidator(filename = "", type = ""):

    result = False

    if (filename):

        result = os.path.exists(filename)

        if (result and type):
            result = filename.endswith("." + type)

    else:
        print(board.labelCollection()["invalid-file"])

    if (filename and not result):
        print(board.labelCollection()["error-label"] + filename + board.labelCollection()["file-not-found"])

    return result


def separator():
    print(board.labelCollection()["separator"])


def titlePress(itemTitle = "", itemVersion = ""):

    programTitle = ""

    if (itemTitle):
        programTitle += str(itemTitle) 

    if (itemVersion):
        programTitle += " " + str(itemVersion)

    if (programTitle):
        print(programTitle)
        separator()
        
