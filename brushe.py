import board
import magic
import paintz
import zipenforcer

def dictionaryMenu(messageOFF = False, menuOptions = False, zipFile = "", dictionaryFile = ""):

    if (not messageOFF):
        print(board.labelCollection()["dictionary-message"])
        paintz.separator()
        menuOptions = True

    if (menuOptions and (zipFile or dictionaryFile)):
        updatedDictionaryMenuItems = board.labelCollection()["dictionary-menuItems"]

        if (zipFile):
            updatedDictionaryMenuItems[0] = updatedDictionaryMenuItems[0].replace(board.labelCollection()["select-file"], zipFile)

        if (dictionaryFile):
            updatedDictionaryMenuItems[1] = updatedDictionaryMenuItems[1].replace(board.labelCollection()["select-file"], dictionaryFile)
        
        paintz.itemPress("", updatedDictionaryMenuItems)

    else: 
        paintz.itemPress("", board.labelCollection()["dictionary-menuItems"])

    dictionaryInput = paintz.menuInput(board.labelCollection()["dictionary-menuSelectionMessage"])
    dictionaryZipFile = zipFile
    dictionaryTextFile = dictionaryFile

    if (paintz.itemPressValidate(dictionaryInput, board.labelCollection()["dictionary-menuItems"])):
            if (int(dictionaryInput) == 1):
                dictionaryZipFile = paintz.menuInput(board.labelCollection()["input-file-name"])

                if (paintz.pathValidator(dictionaryZipFile, "zip")):
                    print(dictionaryZipFile + board.labelCollection()["file-selected"])
                    print(board.labelCollection()["separator"])
                    dictionaryMenu(True, True, dictionaryZipFile, dictionaryFile)
                else:
                    print(board.labelCollection()["separator"])
                    dictionaryMenu(True, True, zipFile, dictionaryFile)

            elif (int(dictionaryInput) == 2):
                
                dictionaryTextFile = paintz.menuInput(board.labelCollection()["input-file-name"])

                if (paintz.pathValidator(dictionaryTextFile, "txt")):
                    print(dictionaryTextFile + board.labelCollection()["file-selected"])
                    print(board.labelCollection()["separator"])
                    dictionaryMenu(True, True, zipFile, dictionaryTextFile)
                else:
                    print(board.labelCollection()["separator"])
                    dictionaryMenu(True, True, zipFile, dictionaryFile)

            elif (int(dictionaryInput) == 3):

                if (paintz.requiredFileValidator([zipFile, dictionaryFile])):
                    print(board.labelCollection()["dictionary-begin"])
                    print(board.labelCollection()["password-label"] + magic.dictionarySpell(zipFile, dictionaryFile))
                    print(board.labelCollection()["separator"])
                    dictionaryMenu(True, True, zipFile, dictionaryFile)
                else:
                    print(board.labelCollection()["dictionary-begin-invalid"])
                    print(board.labelCollection()["separator"])
                    dictionaryMenu(True, True, zipFile, dictionaryFile)

            elif (int(dictionaryInput) == 4):
                print(board.labelCollection()["separator"])
                zipenforcer.menu(True)
                
            elif (int(dictionaryInput) == 5):
                exitProgram()

    else:
        print(board.labelCollection()["dictionary-menuSelectionInvalidMessage"])
        dictionaryMenu(True)


def bruteForceMenu(messageOFF = False, menuOptions = False, zipFile = ""):

    if (not messageOFF):
        print(board.labelCollection()["brute-force-message"])
        paintz.separator()
        menuOptions = True

    if (menuOptions and zipFile):
        updatedBruteForceMenuItems = board.labelCollection()["brute-force-menuItems"]

        if (zipFile):
            updatedBruteForceMenuItems[0] = updatedBruteForceMenuItems[0].replace(board.labelCollection()["select-file"], zipFile)
        
        paintz.itemPress("", updatedBruteForceMenuItems)

    else: 
        paintz.itemPress("", board.labelCollection()["brute-force-menuItems"])

    bruteForceInput = paintz.menuInput(board.labelCollection()["brute-force-menuSelectionMessage"])
    bruteForceZipFile = zipFile

    if (paintz.itemPressValidate(bruteForceInput, board.labelCollection()["brute-force-menuItems"])):
            if (int(bruteForceInput) == 1):
                bruteForceZipFile = paintz.menuInput(board.labelCollection()["input-file-name"])

                if (paintz.pathValidator(bruteForceZipFile, "zip")):
                    print(bruteForceZipFile + board.labelCollection()["file-selected"])
                    print(board.labelCollection()["separator"])
                    bruteForceMenu(True, True, bruteForceZipFile)
                else:
                    print(board.labelCollection()["separator"])
                    bruteForceMenu(True, True, zipFile)

            elif (int(bruteForceInput) == 2):

                if (paintz.requiredFileValidator([zipFile])):
                    print(board.labelCollection()["brute-force-begin"])
                    print(board.labelCollection()["stop-progress"])
                    print(board.labelCollection()["password-label"] + magic.bruteForceSpell(zipFile))
                    print(board.labelCollection()["separator"])
                    bruteForceMenu(True, True, zipFile)
                else:
                    print(board.labelCollection()["brute-force-begin-invalid"])
                    print(board.labelCollection()["separator"])
                    bruteForceMenu(True, True, zipFile)

            elif (int(bruteForceInput) == 3):
                print(board.labelCollection()["separator"])
                zipenforcer.menu(True)

            elif (int(bruteForceInput) == 4):
                exitProgram()

    else:
        print(board.labelCollection()["brute-force-menuSelectionInvalidMessage"])
        bruteForceMenu(True)


def helpMenu(messageOFF = False):

    if (not messageOFF):
        print(board.labelCollection()["help-message"])
        paintz.separator()
        paintz.itemPress("", board.labelCollection()["help-menuItems"])

    helpInput = paintz.menuInput(board.labelCollection()["help-menuSelectionMessage"])

    if (paintz.itemPressValidate(helpInput, board.labelCollection()["help-menuItems"])):
            if (int(helpInput) == 1):
                print(board.labelCollection()["separator"])
                zipenforcer.menu(True)
            elif (int(helpInput) == 2):
                exitProgram()

    else:
        print(board.labelCollection()["help-menuSelectionInvalidMessage"])
        helpMenu(True)


def exitProgram():
    print(board.labelCollection()["exit-message"])
    exit()


def menuSwitcher(input):

    if (str(input) == board.labelCollection()["main-menuItems"][0]):
        dictionaryMenu()
    elif (input == board.labelCollection()["main-menuItems"][1]):
        bruteForceMenu()
    elif (input == board.labelCollection()["main-menuItems"][2]):
        helpMenu()
    elif (input == board.labelCollection()["main-menuItems"][3]):
        exitProgram()
    else:
        print(board.labelCollection()["error-type-1"])